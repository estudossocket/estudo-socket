
const net = require('net');
const socketClient = require('./server-functions');

const servidor = net.createServer( socketClient );

servidor.listen(4181, () => console.log('Servidor rodando na porta 4181') );

servidor.on('error', err => { throw err } );