
var net = require("net");

var Client = net.createConnection;
var client = Client({port: 4181, host: '127.0.0.1'});

client.on('connect', function() {
  console.log('Você está conectado no servidor!');
  client.write('Sou um novo cliente...');
});

client.on('data', (data) => {
  console.log(data.toString());
  // client.end();
});

client.on('end', () => {
  console.log('Você foi desconectado!');
});