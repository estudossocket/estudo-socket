// var arrayClientes = [];
// var ids = 0;

module.exports = function socketClient(cliente) {

    // client.id = ids++
    // arrayClientes.push(cliente);

    cliente.write('Cliente, seja bem vindo!\n\r');
    // cliente.pipe(cliente);

    cliente.on('connect', function () {
        console.log('cliente conectou...')
    })

    cliente.on('data', function (dados) {
        console.log('cliente enviou dados...', dados, dados.toString());
        // function escreveDados(c) { if (!(c == cliente)) c.write(dados.toString()) }
        // arrayClientes.forEach(c => { escreveDados(c) })
    })

    cliente.on('end', function () {
        console.log('cliente desconectado...')
    })

    cliente.setTimeout(60000);
    cliente.on('timeout', () => {
      console.log('Cliente desconectado por ficar 60s sem comunicação');
      cliente.end();
    })

}